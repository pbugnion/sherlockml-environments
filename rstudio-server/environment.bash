#!/bin/bash

set -eu

# Prevent Debconf from prompting for user interaction during package
# installation.
export DEBIAN_FRONTEND=noninteractive

# Install recommended R packages from CRAN repository.
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv-keys 51716619E084DAB9
echo 'deb https://ftp.heanet.ie/mirrors/cran.r-project.org/bin/linux/ubuntu xenial/' | \
    sudo tee -a /etc/apt/sources.list.d/cran.list
sudo apt update
sudo apt install -y r-recommended

# Install RStudio Server from upstream repository.
wget https://download2.rstudio.org/rstudio-server-1.0.136-amd64.deb
sudo apt-get install -y ./rstudio-server-1.0.136-amd64.deb
rm rstudio-server-1.0.136-amd64.deb

# Configure RStudio Server to use /project as the working directory, and bind
# to the correct address and port for SherlockML.
echo "setwd('/project')" > "$HOME"/.Rprofile
echo 'www-address=127.0.0.1' | sudo tee -a /etc/rstudio/rserver.conf
echo 'www-port=8888' | sudo tee -a /etc/rstudio/rserver.conf

# Stop the running Jupyter notebook server, configure the init system to run
# RStudio Server in it's place, and start the RStudio Server service.
sudo sv stop jupyter
sudo rm -fr /etc/service/jupyter
sudo mkdir -p /etc/service/rstudio-server
cat > run <<EOF
#!/bin/sh
set -e
cd /project
exec /sbin/setuser sherlock /usr/lib/rstudio-server/bin/rserver
EOF
chmod 755 run
# Copying the executable run file to /etc/service will start the RStudio server
sudo mv run /etc/service/rstudio-server/run
