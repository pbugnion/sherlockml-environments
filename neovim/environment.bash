#!/bin/bash

set -eu

# Prevent Debconf from prompting for user interaction during package
# installation.
export DEBIAN_FRONTEND=noninteractive

# Add Neovim PPA.
sudo add-apt-repository -y ppa:neovim-ppa/stable
sudo apt-get -y update

# Install Neovim.
sudo apt-get -y install neovim
